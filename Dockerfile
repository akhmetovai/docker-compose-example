FROM jupyter/datascience-notebook:latest

RUN pip install xgboost==1.4.2
RUN pip install sqlalchemy
RUN pip install psycopg2-binary