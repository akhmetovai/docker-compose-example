Данный файл сделан для проверки инструкций из видео Глеба Михайлова
Будет рассмотрен также docker-compose двух контейнеров, с базой postgresql и с ноутбуками, из которых будет доступна база

1. Сначала устанавливаем докер (если еще не установлен)

2. Чтобы сделать пул образа, нужен аккаунт на https://hub.docker.com/
$ docker login
username:
password:

3. Находим нужный образ и качаем его
$ docker pull jupyter/datascience-notebook
$ docker images
> REPOSITORY                     TAG       IMAGE ID       CREATED        SIZE
> jupyter/datascience-notebook   latest    d06dee56948f   36 hours ago   3.94GB

4. Запуск докера
Можно запускать с тегом/либо без тега, но мне такой вариант не очень нравится
$ docker run -p 8888:8888 jupyter/datascience-notebook:latest

Лучше запускать по image_id
$ docker run -p 8888:8888 d06dee56948f

Опционально, можно поменять порт, запустив так:
$ docker run -p 10000:8888 d06dee56948f

Тогда в командной строке меняем 8888 на 10000
http://127.0.0.1:10000/?token=c400d5d32ef21f68fbd558465b47e05a1ea2dda525e9a165

Запущенные докеры можно отслеживать так:
$ docker ps

5. Как залезть в контейнер
$ docker ps
$ docker exec -it 9fb6ea1ab486 bash

6. Как скопировать данные в контейнер
Сначала найдем нужные данные, например wine.data и положим в локальную директорию
https://archive.ics.uci.edu/ml/machine-learning-databases/wine/

$ docker cp wine.data 9fb6ea1ab486:/home/jovyan/wine.data

5. На данном этапе контейнер работает, и мы можем делать наши исследования,
но все данные после перезапуска будут пропадать

Тут нужно понимать, что по умолчанию контейнер запускается из домашней директории,
а мы хотим задать конкретную директорию, из которой мы будем заходить в докер, чтобы
была возможность использовать общие с контейнерами данные

options: --volume, -v

$ docker run -v /home/aydar/docker_course:/home/jovyan/docker_course -p 8888:8888 d06dee56948f

Тут надо не забывать сохранять то, что происходило в докере, так как ноутбук не сохраняет все изменения быстро


6. Установка библиотек.
Чтобы каждый раз не запускать установку библиотек внутри контейнера,
создадим инструкцию для создания образа с необходимыми библиотеками в Dockerfile

FROM jupyter/datascience-notebook:latest

RUN pip install xgboost==1.4.2

Теперь можем сбилдить (сразу с нашим тегом) и запустить:
$ docker build -t ds_jupyter_xgboost .
$ docker run -v /home/aydar/docker_course:/home/jovyan/docker_course -p 8888:8888 ds_jupyter_xgboost

На этом основная часть закончена, далее дополнительно обсуждается docker-compose postgresql + jupyter

7. создаем docker-compose.yml по аналогии с примером из документации docker
https://docs.docker.com/compose/

version: "3.9"  # optional since v1.27.0
services:
  web:
    build: .
    ports:
      - "5000:5000"
    volumes:
      - .:/code
      - logvolume01:/var/log
    links:
      - redis
  redis:
    image: redis
volumes:
  logvolume01: {}

У нас это будет выглядеть следующим образом:

version: "3.9"  # optional since v1.27.0
services:
  jupyter:
    build: .
    ports:
      - "8888:8888"
    volumes:
      - .:/home/jovyan/docker_course

Ругался на версию, нужно было сделать апгрейд docker-compose.
Проверка одного контейнера прошла успешно.

Добавим в композицию еще один контейнер


version: "3.9"  # optional since v1.27.0
services:
  jupyter:
    build:
        context: .
        dockerfile: Dockerfile
    ports:
      - "8888:8888"
    volumes:
      - .:/home/jovyan/docker_course
  db:
    container_name: postgres_container
    image: postgres
    restart: always
    ports:
        - "5432:5432"
    environment:
      POSTGRES_USER: postgres
      POSTGRES_PASSWORD: secret
      PGDATA: /data/postgres
    volumes:
       - pgdata:/data/postgres

volumes:
  pgdata:


Также понадобится скорректировать Dockerfile, добавив библиотеки для коннекта к базе

FROM jupyter/datascience-notebook:latest

RUN pip install xgboost==1.4.2
RUN pip install sqlalchemy
RUN pip install psycopg2

$ docker-compose up --build

Может поругаться на права для чтения, если сохранять volume локально
$ sudo chown -R aydar /home/aydar/docker_course/data/postgres/

8. В ноутбуке в докере проверяем коннект
import pandas as pd
import psycopg2
postgres_conn = psycopg2.connect(host='db', dbname='postgres', user="postgres", password="secret")
pd.read_sql_query('select * from information_schema.tables',con=postgres_conn)

Также вместо хоста host='db' можно поставить ip
$ docker inspect postgres_container
> "IPAddress": "172.18.0.3"

9. Зальем тестовые данные

Проще всего так:
from sqlalchemy import create_engine
engine = create_engine('postgresql://postgres:secret@db:5432/postgres')
df.to_sql('iris_table', engine)

Проверим, что все ок (По умолчанию схема public):
pd.read_sql_query('select * from public.iris_table',con=conn)

Сами данные из базы будут лежать здесь:
$ docker volume ls
> local     docker_course_pgdata

Если понадобится переносить базу, то нужно будет еще дополнительно исхищряться, но для текущих целей это норм
